function lastCar(inventory){
    if(inventory == undefined) {
        return [];
    }
    if(!Array.isArray(inventory)) {
        return [];
    }
   let last =inventory.length-1;
    let ans = inventory[last];
    return ans;
}
module.exports=lastCar;