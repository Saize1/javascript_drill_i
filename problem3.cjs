const inventory =require("./cars.cjs");
function sortArray(inventory){
    if(inventory == undefined) {
        return [];
    }
    if(!Array.isArray(inventory)) {
        return [];
    }
    const sortedList = inventory.sort((a,b) =>
    a.car_model.localeCompare(b.car_model));
    return sortedList;

}
module.exports = sortArray;