function oldCars(inventory,year){
    if(inventory == undefined) {
        return [];
    }
    if(year ==undefined) {
        return [];
    }
    const List =[];
    for(let idx=0;idx<inventory.length;idx++){
        if(!inventory[idx].hasOwnProperty('car_year')){
            continue;
        }
        if(inventory[idx].car_year<2000){
            List.push(inventory[idx].car_year)
        }
    }
    return "The number of cars older than the year 2000: " + List.length +"\n The list of cars older than the year 2000: "+ List;
}
module.exports = oldCars;