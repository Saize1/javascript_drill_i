function years(inventory){
    if(inventory == undefined) {
        return [];
    }
    if(!Array.isArray(inventory)) {
        return [];
    }
    const List =[];
    for(let idx=0;idx<inventory.length;idx++){
    if(!inventory[idx].hasOwnProperty('car_year')){
        continue;
    }
    
List.push(inventory[idx].car_year)
}
return List;
}
module.exports =years;