function carsInfo(inventory,id){
    if(id == undefined) {
        return [];
    }
    if(typeof id != 'number') {
        return [];
    }
    if(inventory == undefined) {
        return [];
    }
    if(!Array.isArray(inventory)) {
        return [];
    }
    for(let idx=0;idx<inventory.length;idx++){
        if(inventory[idx].id ==id){
            return inventory[idx];
        }
       
    }
    return[];
}


module.exports = carsInfo
